var x,y;
var ballSize = 50;
var radius;
var ballSpeedX = 4;
var ballSpeedY = 4;

var mic;
var fft;

function setup() {
  createCanvas(600,400);
  background(0);

  mic = new p5.AudioIn();
  mic.start();
  fft = new p5.FFT();
  mic.connect(fft);

  y=height/2;
  x=ballSize/2;

  radius = x;

  noStroke();
}

function draw() {
  //get colour from sound
  fill(audio());

  //move ellipse
  move();

  //draw ellipse
  ellipse(x, y, ballSize, ballSize);
}

//generates a colour based on the sound
function audio() {
  //get volume
  var vol = mic.getLevel();
  var mappedvol = map(vol, 0, 1, 0,255);

  //get frequenzy
  fft.analyze();
  var hz = fft.getCentroid();
  var mappedhz = map(hz, 1000,5000,0,255);

  return color(mappedhz,mappedvol,(mappedvol+mappedhz)/2);
}

function move() {
  x += ballSpeedX;
  y += ballSpeedY;

  if(x<radius || x>width-radius) {
    ballSpeedX *= -1;
  }

  if(y<radius || y>height-radius) {
    ballSpeedY *= -1
  }
}
